import random
from bidict import bidict

# to find: 
# 1. position of the rotors
# 2. starting position of the rotors
# 3. missing connections in the patchbay (only 1 since the usual number is 10?) maybe the F one?
# se sparo il cythertext dentro devo uscire con il plain

cypertext =  "dvgs".upper()#{atrpwb_pxr_mwqlqrxsqggc_crsrv_xiwdtyu_fdp}".capitalize()
plaintext =  "ping".upper()
rotors    = ["BDFHJLCPRTXVZNYEIWGAKMUSQO", "AJDKSIRUXBLHWTMCQGZNPYFVOE", "EKMFLGDQVZNTOWYHXUSPAIBRCJ"]
reflector =  "EJMZALYXVBWFCRQUONTSPIKHGD"
alphabet  =  "ABCDEFGHIJKLMNOPQRSTUVWXYZ" #26 letters
plugboard =  "WORXZHINAVSBDLGCKMPPQQEETTUUJJYYF?" # missing = "QETUFJPY"

# randomize position of the notch

def split_pairs(text):
    return dict({alphabet[i]: text[i] for i in range(0, len(text))})

rotors = [split_pairs(r) for r in rotors]
reflector = split_pairs(reflector)
plugboard = bidict({plugboard[i]: plugboard[i+1] for i in range(0, len(plugboard),2)})

def swap(letter: str, dictionary: dict):
    """simply apply the dictionary to the letter"""
    if letter in dictionary.keys():
        return dictionary[letter]
    elif dictionary is bidict:
        return dictionary.inverse[letter]
    return letter


rot_rotations = [0,0,0] # offsets of the rotors
def rotate_rotor(rot, rot_prec = 0):
    """rotate the rotor and return the new position"""
    if rot_prec % len(alphabet) == 0:
        rot += 1
        rot_prec = 0 # to not make it explode
    return rot, rot_prec


def rotors_connection(letter, r1, r2, rot_rotation):
    """simulates the connection between two rotors"""
    values = list(r1.values())
    idx = (values.index(letter) + rot_rotation) % len(alphabet)

    keys = list(r2.keys())
    letter = keys[idx]

    return letter

conf_ok = 0
tried_configs = [([0,1,2] + rot_rotations)]
permutation = [0,1,2]

while True:
    if not conf_ok:

        # choose rotor order
        random.shuffle(permutation)
        rotors = [rotors[i] for i in permutation]
        rot_rotations = [random.randint(0,25), random.randint(0,25), random.randint(0,25)]
        curr_config = (permutation + rot_rotations)

        # choose rotor starting point
        while curr_config in tried_configs:
            random.shuffle(permutation)
            rotors = [rotors[i] for i in permutation]
            rot_rotations = [random.randint(0,25), random.randint(0,25), random.randint(0,25)]
            curr_config = (permutation + rot_rotations)

        tried_configs.append(curr_config)

    
    for i in range(0,len(cypertext)):

        # take letter of input
        letter = cypertext[i]

        # apply plugboard
        letter = swap(letter,plugboard)

        # rotate rotor 1
        rot_rotations[0] +=1 #, _ = rotate_rotor(rot_rotations[0])

        # apply rotor 1
        letter = swap(letter,rotors[0])

        # connection between rotor 1 and 2
        letter = rotors_connection(letter, rotors[0], rotors[1], rot_rotations[0])

        # rotate rotor 2
        #rot_rotations[1], rot_rotations[0] = rotate_rotor(rot_rotations[1], rot_rotations[0])

        # apply rotor 2
        letter = swap(letter,rotors[1])

        # connection between rotor 2 and 3
        letter = rotors_connection(letter, rotors[1], rotors[2], rot_rotations[1])

        # apply rotor 3
        letter = swap(letter,rotors[2])

        # rotate rotor 3 conditionally  (this will never happen since our input is too little)
        #rot_rotations[2], rot_rotations[1] = rotate_rotor(rot_rotations[2], rot_rotations[1])

        # connection between rotor 3 and reflector
        letter = rotors_connection(letter, rotors[2], reflector, rot_rotations[2])

        # apply reflector
        letter = swap(letter,reflector)

        ## everythink in reverse order (no rotations)

        # connection between rotor reflector and rotor 3
        letter = rotors_connection(letter, reflector, rotors[2], rot_rotations[2])

        # apply rotor 3
        letter = swap(letter,rotors[2])

        # connection between rotor 3 and 2
        letter = rotors_connection(letter, rotors[2], rotors[1], rot_rotations[2])

        # apply rotor 2
        letter = swap(letter,rotors[1])

        # connection between rotor 2 and 1
        letter = rotors_connection(letter, rotors[1], rotors[0], rot_rotations[2])

        # apply rotor 1
        letter = swap(letter,rotors[0])

        # apply plugboard
        letter = swap(letter,plugboard)

        # check if letter is correct
        if letter == plaintext[i]:
            conf_ok += 1
            if(conf_ok == len(cypertext)):
                print(curr_config)
                break
        else:
            conf_ok = False
            break