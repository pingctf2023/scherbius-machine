from pyenigma import enigma
from pyenigma import rotor

def bruteforce_enigma(ciphertext, reflector, rotor1, rotor2, rotor3):
    for key1 in range(26):
        for key2 in range(26):
            for key3 in range(26):
                key = chr(65 + key1) + chr(65 + key2) + chr(65 + key3)
                for plug_letter in [chr(65 + i) for i in range(26) if chr(65 + i) not in key + "FH"]:
                    plugboard_config = f"AV BS CG DL HZ IN KM OW RX {plug_letter}F"
                    engine = enigma.Enigma(reflector, rotor3, rotor2, rotor1, key=key, plugs=plugboard_config)
                    decrypted_message = engine.encipher(ciphertext)
                    if "ping" in decrypted_message[:4]:
                        print(f"Key: {key}, Plugboard: {plugboard_config}, Decrypted Message: {decrypted_message}")

# Enigma Settings
reflector = rotor.ROTOR_Reflector_A
rotor1 = rotor.ROTOR_I
rotor2 = rotor.ROTOR_II
rotor3 = rotor.ROTOR_III

# Encrypted Message
ciphertext = "dvgs{atrpwb_pxr_mwqlqrxsqggc_crsrv_xiwdtyu_fdp}"

# Brute-force the key
bruteforce_enigma(ciphertext, reflector, rotor1, rotor2, rotor3)