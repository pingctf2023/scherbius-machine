# scherbius-machine
Our team intercepted an machine from the enemy, but it suffered damage during transport, causing rotor and plugboard disarray. Your mission is to reconstruct the machine configuration, determine the missing plugboard connection, and decrypt the given ciphertext.

Key components:
- Rotors: BDFHJLCPRTXVZNYEIWGAKMUSQO, AJDKSIRUXBLHWTMCQGZNPYFVOE, EKMFLGDQVZNTOWYHXUSPAIBRCJ
- Reflector: EJMZALYXVBWFCRQUONTSPIKHGD
- Partial plugboard image provided.
- Ciphertext: `dvgs{atrpwb_pxr_mwqlqrxsqggc_crsrv_xiwdtyu_fdp}`

<div style="text-align:center"><img src="machine-image.png" width="60%" /></div>

# idea behind
Looking at the image i was sure this was an enigma machine, and in fact, Scherbius was the inventor of the commercial version of the enigma machine. I knew only the basics about this machine, so i had to learn more about this fascinating contraption. here is a list of the videos i watched to educate myself on it:
- [158,962,555,217,826,360,000 (Enigma Machine)](https://youtu.be/G2_Q9FoD-oQ)
- [How did the Enigma Machine work?](https://youtu.be/ybkkiGtJmkM)
- [Flaw in the Enigma Code](https://youtu.be/V4V2bpZlqx8)
- [Turing's Enigma Problem (Part 1)](https://youtu.be/d2NWPG2gB_A)
- [Tackling Enigma (Turing's Enigma Problem Part 2)](https://youtu.be/kj_7Jc1mS9k)

The Enigma machine was an electromechanical encryption device used by the German military during World War II. It employed a series of rotors, each with an alphabet ring, to encrypt messages. When a key was pressed, an electrical current passed through the rotors, creating a polyalphabetic substitution cipher. The machine's settings, including rotor positions and plugboard connections, changed daily, adding a layer of complexity.

So after analyzing the videos, i understood that the "Rotors" given by the challenge are formatted to represent an alphabetical mapping from A to Z, for example the first: A→B, B→D, F→H etc... . Same for the reflector.

Usually 10 of the 12 plugboard connections were used, so only one plugboard substitiution had to be found. However, the first part of the cyphertext is `dvgs` and the related plaintext is `ping`, due to the format of the flag. So for the bruteforcing of the first letters is not necessary to discover the mapping of `F`.
I organized the data in dictionaries (or bidict in the case of `plugboard`) and i created some functions:
- `rotate_rotor`: rotates a rotor based on the rotation of the precedent,
- `rotors_connection`: simulates the swapping which happens between two rotors
- `swap`: utility function to apply dictionaries

The code then proceeds in a loop to brudeforce the first 4 letters:

0. first way

   0. take letter of input
   0. apply plugboard
   0. rotate rotor 1
   0. apply rotor 1
   0. connection between rotor 1 and 2
   0. rotate rotor 2 conditionally
   0. apply rotor 2
   0. connection between rotor 2 and 3
   0. apply rotor 3
   0. rotate rotor 3 conditionally  (this will never happen since our input is too little)
   0. connection between rotor 3 and reflector
   0. apply reflector
0. way back: everything in reverse order (but no rotations)

   0. connection between reflector and rotor 3
   0. apply rotor 3
   0. connection between rotor 3 and 2
   0. apply rotor 2
   0. connection between rotor 2 and 1
   0. apply rotor 1
   0. apply plugboard
0. check if letter is correct

   0. if it is, keep going, otherwise discard the configuration

> for more details the code is implemented in `solution.py`

Unfurtunately my approach resulted too long of a bruteforcing, and it did not find the combination of rotors.
The official solution (provided in `pynigma-solution.py`) uses a library so the code is really compact (26 lines). I didn't even consider using a library because it would have made me learn nothing about the enigma machine.
Executing the official solution we find many combination which brings us the first 4 letters as `ping`:
 - ping{fnigma_ist_uaszinifrfnd_finfn_schonfv_vag}
 - ping{enigma_ist_uaszinierend_einen_schonev_jag}
 - ping{enigfa_ist_uaszinierend_einen_schonev_yag}
 - ping{enigma_ist_uasbinierenf_einen_schonev_pag}
 - ping{enigma_ist_naszinierend_einen_schonev_aag}
 - ping{enigma_ist_uaszinierend_einen_schonev_bag}
 - ping{enigma_ist_uahzpnieyend_einen_schonev_cag}
 - ping{envgma_isn_uasziqierend_ennfn_schonev_oag}
 - ping{ebigma_isf_uaszinierend_einen_schojev_uag}
 - ping{enigma_ist_faszinierend_einen_schonen_tag}
 - ping{enigva_ist_ujszinierend_einen_scjfnev_xag}
 - ping{enigma_iwt_uaszinuefend_einen_ichonev_zag}
 - ping{enigma_ist_uaszinierend_einen_schonxv_mag}
 - ping{enigma_ist_uaszinierend_einen_scfonev_iag}
 
but the one which makes most sense is ping{enigma_ist_faszinierend_einen_schonen_tag}. that's the flag 🥳.